package az.ingress.auth.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.micrometer.common.util.StringUtils;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

public class JwtService {

    private String keyStr = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIgYWRtaW4iLCJuYW1lIjoiQWRtaW4gVXNlciIsInNvbWVLZXkiOiJIZWxsbyBKV1QgIiwiaWF0IjoxNzE2MDE0Njk0LCJleHAiOjE3MTYwMTQ5OTR9.Q6eS0ajKDtTC9P5CC3X9mPKJVhqxe_N7vM_Btra3NHE";

    private Key key;

    public void init() {
        byte[] keyBytes = keyStr.getBytes();
        key = Keys.hmacShaKeyFor(keyBytes);
//        if (StringUtils.isBlank(properties.getJwtProperties().getSecret())) {
//            throw new RuntimeException("Token config not found");
//        }
    }

    public String issueToken() {
        init();
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(" admin")
                .claim("name", "Admin User")
                .claim("someKey", "Hello JWT ")
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration
                        .ofSeconds(300))))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }

    public Claims parseToken(String token) {
        init();
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

}
