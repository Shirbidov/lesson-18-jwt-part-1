package az.ingress.auth.jwt;

import az.ingress.auth.jwt.JwtService;
import io.jsonwebtoken.Claims;

public class Main {
    public static void main(String[] args) {

        JwtService jwtService = new JwtService();
        final String jwt = jwtService.issueToken();
        System.out.println("JWT : " +  jwt);

        final Claims claims = jwtService.parseToken
        ("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIgYWRtaW4iLCJuYW1lIjoiQWRtaW4gVXNlciIsInNvbWVLZXkiOiJIZWxsbyBKV1QgIiwiaWF0IjoxNzE2MTI3NTk4LCJleHAiOjE3MTYxMzA1OTh9.PWT6uVnI-RJ3w8VxSibEDaE8RYOPzasYMc1z1hyUsEM");
        System.out.println("Claims from jwt " + claims);
    }
}
