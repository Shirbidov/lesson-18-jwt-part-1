package az.ingress.ms24;

import az.ingress.ms24.domin.UserEntity;
import az.ingress.ms24.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class UserManagmentMs implements CommandLineRunner {
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(UserManagmentMs.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        UserEntity userEntity
                = UserEntity.builder()
                .username("user")
                // .authorities(List.of())
                .password("{noop}1234")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .enabled(true)
                .credentialsNonExpired(true)
                .build();

       // userRepository.save(userEntity);
    }
}
