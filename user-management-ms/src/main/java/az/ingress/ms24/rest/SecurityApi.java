package az.ingress.ms24.rest;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class SecurityApi {


    @GetMapping("/public")
    public String sayHello() {
        return "Hello world";
    }

    @GetMapping("/authenticated")
    public String sayHelloAuthenticated(Authentication authentication) {
        return "Hello, " + authentication;
    }

    @GetMapping("/role-user")
    public String sayHelloRoleUser() {
        return "Hello world role-user";
    }

    @GetMapping("/role-admin")
    public String sayHelloRoleAdmin() {
        return "Hello world role-admin";
    }
}
