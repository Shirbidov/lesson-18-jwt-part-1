package az.ingress.service;

public interface TransferService {

    void transfer(Long sourceAccountId, Long targetAccountId, Double amount);

}
