package az.ingress.service.dto;

import lombok.Data;

@Data
public class CourseDto {

    private Long id;

    private String name;

}
