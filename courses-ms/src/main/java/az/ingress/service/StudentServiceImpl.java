package az.ingress.service;

import az.ingress.service.dto.StudentDto;
import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import az.ingress.domain.StudentEntity;
import az.ingress.repo.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    private final EntityManager entityManager;

    @Override
    @Transactional(rollbackFor = Exception.class, readOnly = true)
    public void testTransaction() throws Exception {
        // actualMethod();
        int a = 6;
        String myCoolAnimal = new String("Dog"); //strong
        SoftReference<String> softReference = new SoftReference<>("DOG1"); //OOM
        softReference.get(); //null

        WeakReference<String> weakReference = new WeakReference<>("DOG2"); //Weak
        weakReference.get();

        ReferenceQueue<String> referenceQueue = new ReferenceQueue<>();
        PhantomReference<String> phantomReference = new PhantomReference<>("Dog3", referenceQueue);
        phantomReference.get(); //null
        studentRepository.findById(5L);
    }

    @Override
    @Transactional
    public List<StudentDto> listStudents() {
        // EntityGraph entityGraph = entityManager.getEntityGraph("students-with-courses");

        EntityGraph entityGraph = entityManager.createEntityGraph(StudentEntity.class);
        entityGraph.addAttributeNodes("courses");
        Map<String, Object> properties = new HashMap<>();
        properties.put("jakarta.persistence.fetchgraph", entityGraph);
        StudentEntity student = entityManager.find(StudentEntity.class, 1L, properties);

        return List.of(modelMapper.map(student, StudentDto.class));
    }



    public void actualMethod() throws Exception {
        StudentEntity studentEntity =
                StudentEntity.builder()
                        .firstName("Senan")
                        .lastName("Abdullayev")
                        .build();
        studentRepository.save(studentEntity);
        //if (1 == 1)
        // throw new Exception("Something failed");
        studentRepository.deleteById(5L);
    }
}
