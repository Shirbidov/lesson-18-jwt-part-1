package az.ingress.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import az.ingress.domain.AccountEntity;
import az.ingress.repo.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TransferServiceImpl implements TransferService {

    private final AccountRepository accountRepository;

    @Override
    @Transactional
    @SneakyThrows
    public void transfer(Long sourceAccountId, Long targetAccountId, Double amount) {
        AccountEntity sourceAccount;
        AccountEntity targetAccount;

        System.out.println("Locking first account " + sourceAccountId);
        sourceAccount = accountRepository.findById(sourceAccountId)
                .orElseThrow();
        System.out.println("First account locked " + sourceAccount.getOwner());
       // Thread.sleep(10_000);
        System.out.println("Locking second account " + targetAccountId);
        targetAccount = accountRepository.findById(targetAccountId)
                .orElseThrow();
        System.out.println("Second account locked " + targetAccount.getOwner());

//        if (sourceAccountId < targetAccountId) {
//            sourceAccount = accountRepository.findById(sourceAccountId)
//                    .orElseThrow();
//            Thread.sleep(10_000);
//            targetAccount = accountRepository.findById(targetAccountId)
//                    .orElseThrow();
//        } else {
//            targetAccount = accountRepository.findById(targetAccountId)
//                    .orElseThrow();
//            Thread.sleep(10_000);
//            sourceAccount = accountRepository.findById(sourceAccountId)
//                    .orElseThrow();
//        }


        if (sourceAccount.getBalance() >= amount) {
            targetAccount.setBalance(targetAccount.getBalance() + amount);
            Thread.sleep(30_000);
            sourceAccount.setBalance(sourceAccount.getBalance() - amount);
            //   accountRepository.save(targetAccount);
            //   accountRepository.save(sourceAccount);
        }
    }
}
