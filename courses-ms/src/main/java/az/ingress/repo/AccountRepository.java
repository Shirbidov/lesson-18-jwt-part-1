package az.ingress.repo;

import jakarta.persistence.LockModeType;
import az.ingress.domain.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    @Lock(LockModeType.OPTIMISTIC)
    Optional<AccountEntity> findById(Long id);
}
