package az.ingress;

import az.ingress.domain.CourseEntity;
import az.ingress.domain.StudentEntity;
import az.ingress.repo.AccountRepository;
import az.ingress.repo.StudentRepository;
import az.ingress.service.L1CacheTest;
import az.ingress.service.StudentService;
import az.ingress.service.TransferService;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.hibernate.cache.spi.RegionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication
@RequiredArgsConstructor
public class CourseManagementMs implements CommandLineRunner {

    RegionFactory regionFactory;
    private final StudentService service;
    private final StudentRepository studentRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final L1CacheTest l1CacheTest;
    private final AccountRepository accountRepository;
    private final TransferService transferService;
    // private final StudentRepository studentRepository;


    public static void main(String[] args) {
        SpringApplication.run(CourseManagementMs.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // System.out.println(service.listStudents());
//        for (int i = 0; i < 100; i++) {
//            studentRepository.save(studentEntity());
//        }

        //   l1CacheTest.loadData();

//        AccountEntity sourceAccount =
//                AccountEntity
//                        .builder()
//                        .owner("Atilla")
//                        .balance(200D)
//                        .build();
//
//        AccountEntity targetAccount =
//                AccountEntity
//                        .builder()
//                        .owner("Kamil")
//                        .balance(200D)
//                        .build();
//
//        accountRepository.save(sourceAccount);
//        accountRepository.save(targetAccount);

         transferService.transfer(1L, 2L, 100D);

//        Thread thread = new Thread(() -> transferService.transfer(1l, 2l, 200D));
//        Thread thread2 = new Thread(() -> transferService.transfer(2l, 1l, 200D));
//
//        System.out.println("Transfer started");
//        thread.start();
//        thread2.start();
//        thread2.join();
//        thread.join();
//        System.out.println("Transfer completed");
    }


    private StudentEntity studentEntity() {
        StudentEntity student = StudentEntity
                .builder()
                .firstName("First Name")
                .lastName("LastName")
                .build();
        CourseEntity courseEntity =
                CourseEntity.builder()
                        .student(student)
                        .name("MS")
                        .build();

        CourseEntity courseEntity2 =
                CourseEntity.builder()
                        .student(student)
                        .name("OCA")
                        .build();

        student.setCourses(List.of(courseEntity2, courseEntity));
        return student;
    }
}
