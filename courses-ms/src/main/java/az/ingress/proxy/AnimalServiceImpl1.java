package az.ingress.proxy;

import org.springframework.stereotype.Component;

@Component
public class AnimalServiceImpl1 implements AnimalService {
    @Override
    @LogExecutionTime
    public void makeNoise() {
        System.out.println("Bark");
    }
}
