package az.ingress.proxy;

import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TransactionalAspect {

//    @Pointcut("execution(public * org.example.proxy.*.makeNoise(..))")
//    private void publicMethodsFromLoggingPackage() {
//    }

    @SneakyThrows
    @Around("@annotation(org.example.proxy.LogExecutionTime)")
    public void methodBeforeAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("Animal will bark now");

        proceedingJoinPoint.proceed();
        System.out.println("Animal stopped barking");

    }
}
