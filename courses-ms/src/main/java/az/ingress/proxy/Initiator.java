package az.ingress.proxy;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Initiator {

    private final AnimalService animalService;

    @PostConstruct
    public void init() {
        animalService.makeNoise();
    }
}
